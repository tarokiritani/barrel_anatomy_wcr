# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 16:48:18 2017

@author: kiritani
"""

def main():
    
    import sqlite3
    from pandas import read_sql_query
    import numpy as np
    from scipy.io import loadmat, savemat
    import collections
    import pdb
    from collections import OrderedDict
    import pandas as pd
    from shutil import copyfile
    import os
    
    dbpath = 'C:/Users/kiritani/Documents/data/GitHub/experiments/db/development.sqlite3'
    c = sqlite3.connect(dbpath)
    sql = """
    SELECT analyses.id, file, analysis_type, cell_id, cell_type, brain_area,
    layer, pipette_resistance, depth, number, date_of_birth, sex, species_strain,
    confirmedlayer, confirmedcolumn FROM analyses INNER JOIN cells ON
    analyses.cell_id = cells.id INNER JOIN mice ON cells.mouse_id = mice.id WHERE
    analysis_type IN('free whisking', 'active touch')
    """
    df_ephys = read_sql_query(sql, c)
    df_anatomy = read_sql_query("""
    SELECT cell_id, file FROM analyses WHERE analysis_type = 'histology'
    """,
    c)
    c.close()
    df_anatomy = df_anatomy.rename(columns={'file': 'Cell_AnatomyFileNames'})
    df = pd.merge(df_ephys, df_anatomy, how='left', on='cell_id')
    df['Cell_AnatomyFileNames'].fillna('', inplace=True)
    
    
    key_names = {
        'number': 'Mouse_Name',
        'date_of_birth': 'Mouse_DateOfBirth',
        'species_strain': 'Mouse_Genotype',
        'sex': 'Mouse_Sex',
        'pipette_resistance': 'Cell_PipetteResistance',
        'cell_type': 'Cell_tdTomatoExpressing',
        'brain_area': 'Cell_TargetedBrainArea',
        'layer': 'Cell_Layer',
        'depth': 'Cell_Depth',
        'cell_id': 'Cell_ID',
        'analysis_type': 'Trial_Type',
        'confirmedlayer': 'Cell_AnatomicalLayer',
        'confirmedcolumn': 'Cell_AnatomicalColumn',
        'file': 'files'
    }
    df = df.rename(columns=key_names)
    df = df.assign(Trial_MembranePotential=None, Trial_Piezo=None, 
                   Trial_WhiskerAngle_C2_right=None, Trial_ContactTime=None,
                   Trial_Spikes=None, Cell_Counter=0,
                   Trial_Piezo_SamplingRate=None, Trial_StartTime=None,
                   Trial_MembranePotential_SamplingRate=None, Trial_Counter=None)
    
    for index, row in df.iterrows():
#        mat = {}
#        for file in row['files'].split(';'):
#            if file.endswith(('.mat', '.xsg', '.signal')):
#                mat.update(loadmat(file, struct_as_record=False, squeeze_me=True))
#        df.set_value(index, 'Trial_StartTime', mat['header'].xsgFileCreationTimestamp)
#        df.set_value(index, 'Trial_MembranePotentialSamplingRate', mat['header'].ephys.ephys.sampleRate)
        df.set_value(index, 'Trial_WhiskerAngle_C2_right_SamplingRate', 500)
#        df.set_value(index, 'Trial_MembranePotential', mat['data'].ephys.trace_1)
#        if isinstance(mat['peakTiming'], collections.Iterable):
#            df.set_value(index, 'Trial_Spikes', mat['peakTiming'])
#        else:
#            df.set_value(index, 'Trial_Spikes', [mat['peakTiming']])    
#        
#        if row.Trial_Type == 'active touch':
#            try:
#                df.set_value(index, 'Trial_Piezo', mat['data'].acquirer.trace_2)
#                df.set_value(index, 'Trial_PiezoSamplingRate', mat['header'].ephys.ephys.sampleRate)
#            except AttributeError:
#                pass
#        
#        df.set_value(index, 'Trial_WhiskerAngle_C2_right', mat['angleArray'])
#        if 'onOffTiming' in mat.keys():
#            df.set_value(index, 'Trial_ContactTime', mat['onOffTiming'])
#        
        if row.Mouse_Sex == 'male':
            df.set_value(index, 'Mouse_Sex', 'm')
        else:
            df.set_value(index, 'Mouse_Sex', 'f')
        
        if row['Cell_tdTomatoExpressing'] == 'tdTomato':
            df.set_value(index, 'Cell_tdTomatoExpressing', True)
        else:
            df.set_value(index, 'Cell_tdTomatoExpressing', False)
        
        if row.Cell_Depth < 385:
            df.set_value(index, 'Cell_Layer', 'L2/3')
        else:
            df.set_value(index, 'Cell_Layer', 'L4')
        
    df.Mouse_Name = df.Mouse_Name.astype(str)
    df.Mouse_Name = 'TK' + df.Mouse_Name
    df.Mouse_DateOfBirth = pd.to_datetime(df.Mouse_DateOfBirth)
    #df['Trial_StartTime'] = pd.to_datetime(df['Trial_StartTime'])
    df = df.sort_values(['Mouse_Name', 'Cell_ID'])
    for _, mouse_df in df.groupby('Mouse_Name'):
        counter = 1
        for __, cell_df in mouse_df.groupby('Cell_ID'):
            df.set_value(cell_df.index, 'Cell_Counter', counter)
            counter += 1
            
    order = ['Mouse_Name', 'Mouse_DateOfBirth', 'Mouse_Sex', 'Mouse_Genotype',
        'Cell_ID', 'Cell_Counter', 'Cell_PipetteResistance', 'Cell_Layer',
        'Cell_TargetedBrainArea', 'Cell_AnatomicalLayer', 'Cell_AnatomicalColumn',
        'Cell_Depth', 'Cell_tdTomatoExpressing', 'Cell_AnatomyFileNames', 'Trial_Counter',
        'Trial_Type', 'Trial_StartTime',
        'Trial_WhiskerAngle_C2_right', 'Trial_WhiskerAngle_C2_right_SamplingRate',
        'Trial_Piezo', 'Trial_Piezo_SamplingRate','Trial_MembranePotential',
        'Trial_Spikes',
        'Trial_MembranePotential_SamplingRate', 'Trial_ContactTime', 'id', 'files'
    ]
    
    df = df[order]
    
    for index, row in df.iterrows():
        #cell_id = row.cell_id
        destfiles = ''
        for fname in row.Cell_AnatomyFileNames.split(';'):
            if os.path.isfile(fname):
                _, extension = os.path.splitext(fname)
                if extension == '.roi':
                    c = str(row.Cell_Counter) + '_'
                else:
                    c = ''
                destfile = 'C://Users/kiritani/desktop/mice/' + row.Mouse_Name + '/' + row.Mouse_Name + '_' + c + os.path.basename(fname);
                if destfiles == '':
                    sep = ''
                else:
                    sep = ';'
                destfiles = destfiles + sep + os.path.basename(destfile)
                if not os.path.isfile(destfile):
                    try:
                        os.mkdir(os.path.dirname(destfile))
                    except(FileExistsError):
                        pass
                    copyfile(fname, destfile)
        df.set_value(index, 'Cell_AnatomyFileNames', destfiles)
    
    data_description = dict()
    data_description['Mouse_Name'] = {
        'type': 'TKnnn: TK = Taro Kiritani, nnn = mouse number. Unique label in Carl Petersen lab'
    }
    data_description['Mouse_DateOfBirth'] = {
        'type': 'Day of the mouse birth',
        'unit': 'Year, Month, Day'
    }
    data_description['Mouse_Sex'] = {
        'type': 'm = male, f = female'
    }
    data_description['Mouse_Genotype'] = {
        'type': ''
    }
    data_description['Cell_ID'] = {
        'type': 'Unique identifier of cells'
    }
    data_description['Cell_Counter'] = {
        'type': 'Count of neurons for a mouse'
    }
    data_description['Cell_PipetteResistance'] = {
        'type': 'Pipette resistance',
        'unit': 'MOhm'
    }
    data_description['Cell_Layer'] = ''
    data_description['Cell_TargetedBrainArea'] = ''
    data_description['Cell_Depth'] = {
        'type': 'Cell depth from the surface under 2 photon microscope',
        'unit': 'um'
    }
    data_description['Cell_AnatomicalLayer'] = {
            'type': 'Layer confirmed with imaging'
    }
    data_description['Cell_AnatomicalColumn'] = {
            'type': 'Column confirmed with imaging'
    }
    data_description['Cell_tdTomatoExpressing'] = {
        'type': '0 for false 1 for true'
    }
    data_description['Cell_AnatomyFileNames'] = {
        'type': 'Paths for anatomy files'
    }
    data_description['Trial_Counter'] = {
            'type': 'Count of trials for a neuron'
    }
    data_description['Trial_Type'] = {
        'type': '"free whisking" or "active touch"'
    }
    data_description['Trial_WhiskerAngle_C2_right'] = {
        'type': 'Right C2 whisker angle tracked from high speed video by semi-automated routines',
        'unit': 'degree'
    }
    data_description['Trial_WhiskerAngle_C2_right_SamplingRate'] = {
        'type': 'Sampling frequency, or frame rate for whisker filming',
        'unit': 'fps'
    }
    data_description['Trial_StartTime'] = {
        'type': 'Time when each trial started',
        'unit': 'Year, Month, Day, Hour, Minute, Second'
    }
    data_description['Trial_ContactTime'] = {
        'type': 'Timing of onsets and offsets for whicker contact on piezo',
        'unit': 'second'
    }
    data_description['Trial_Piezo'] = {
        'type': 'signal from a piezo sensor used for whisker contact detection',
        'unit': 'V'
    }
    data_description['Trial_Piezo_SamplingRate'] = {
        'type': 'Sampling frequency for the piezo data',
        'unit': 'Hz'
    }
    data_description['Trial_MembranePotential'] = {
        'type': 'membrane potential recording',
        'unit': 'mV'
    }
    data_description['Trial_MembranePotential_SamplingRate'] = {
        'type': 'Sampling freqneucy for the WCR data',
        'unit': 'Hz'
    }
    data_description['Trial_Spikes'] = {
        'type': 'spike peak timing',
        'unit': 'second'
    }
   # pdb.set_trace()

#    dfmat = df.drop(['Trial_WhiskerAngle_C2_right', 'Trial_WhiskerAngle_C2_right_SamplingRate',
#        'Trial_Piezo', 'Trial_PiezoSamplingRate','Trial_MembranePotential',
#        'Trial_MembranePotentialSamplingRate', 'Trial_ContactTime', 'Trial_Spikes'], 1)
    df.to_csv('C:/Users/kiritani/Desktop/datacsv.csv')
   # df.Mouse_Genotype = [tuple(gt.split('/')) for gt in df.Mouse_Genotype]
    
    data_description['TrialMembS'] = data_description.pop('Trial_MembranePotential_SamplingRate')
    data_description['TrialWhiskS'] = data_description.pop('Trial_WhiskerAngle_C2_right_SamplingRate')
    order = ['TrialMembS' if x == 'Trial_MembranePotential_SamplingRate' else x for x in order]
    order = ['TrialWhiskS' if x == 'Trial_WhiskerAngle_C2_right_SamplingRate' else x for x in order]
    data_description = OrderedDict([(o, data_description[o]) for o in order[:-2]])
    savemat('C:/Users/kiritani/Desktop/datadescription.mat', {'data_description': data_description}, oned_as='column')

if __name__ == "__main__":
    main()