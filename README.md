# barrel_anatomy_WCR

Code for Hesam Setareh and Taro Kiritani's project on the anatomical and WCR data in the mouse barrel cortex. We first analyze the data Taro Kiritani acquired in [Petersen Lab](http://lsens.epfl.ch/) at [Ecole Polytechnique Federale de Lausanne](http://www.epfl.ch/), Switzerland. Then, we aim to build a computational model of the barrel cortex.

To view my data interactively, please visit Taro's website: http://www.brain-recording.science