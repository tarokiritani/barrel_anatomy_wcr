function csv2dataset
    T = readtable('C:\users\kiritani\desktop\datacsv.csv');
    whiskers = cell(height(T), 1);
    ephysTraces = cell(height(T), 1);
    ephysSampleRate = cell(height(T), 1);
    piezos = cell(height(T), 1);
    piezosSampleRate = cell(height(T), 1);
    onOffs = cell(height(T), 1);
    genotypes = cell(height(T), 1);
    trial_time = cell(height(T), 1);
    
    for row = 1:height(T)
        files = strsplit(T.files{row}, ';');
        clearvars -except T whiskers ephysTraces ephysSampleRate piezos ...
            piezosSampleRate onOffs genotypes trial_time row files
        for file = files
            try
                load(file{1}, '-mat')
            catch
            end
        end
        if exist('angleArray', 'var')
            s = size(angleArray);
            if s(1) == 1
                angleArray = angleArray';
            end
            whiskers{row} = angleArray;
        end
        
        try
            ephysTraces{row} = data.ephys.trace_1;
            ephysSampleRate{row} = header.ephys.ephys.sampleRate;
        catch
        end

        try
            if strcmp(T.Trial_Type{row}, 'active touch')
                piezos{row} = data.acquirer.trace_2;
                piezosSampleRate{row} = header.acquirer.acquirer.sampleRate;
            end
        catch
        end

        if exist('onOffTiming', 'var')
            onOffs{row} = onOffTiming;
        else
        end

        gtype = T.Mouse_Genotype{row};
        delimiter = findstr(gtype, '/');
        genotypes{row} = {gtype(1:delimiter(end) - 1), gtype(delimiter(end) + 1:end)};
        trial_time{row} = datevec(header.xsgFileCreationTimestamp);
    end

    T.Trial_WhiskerAngle_C2_right = whiskers;
    T.Trial_WhiskerAngle_C2_right_SamplingRate = ones(row, 1) * 500;
    T.Trial_MembranePotential = ephysTraces;
    T.Trial_MembranePotential_SamplingRate = ephysSampleRate;
    T.Trial_Piezo = piezos;
    T.Trial_Piezo_SamplingRate = piezosSampleRate;
    T.Trial_ContactTime = onOffs;
    dobs = datevec(T.Mouse_DateOfBirth);
    T.Mouse_DateOfBirth = dobs(:, 1:3);
    T.Trial_StartTime = cell2mat(trial_time);
    T.Mouse_Genotype = genotypes;
    T = sortrows(T, [2, 7, 17]);
    trial_counter = ones(height(T), 1);
    for row = 2:height(T)
        if strcmp(T.Mouse_Name{row - 1}, T.Mouse_Name{row}) && T.Cell_Counter(row - 1) == T.Cell_Counter(row)
            trial_counter(row, 1) = trial_counter(row-1, 1) + 1;
        end
    end
    T.Trial_Counter = trial_counter;
    T.Var1 = [];
    T.id = [];
    T.files = [];
    data = table2struct(T, 'ToScalar', true);
    load('C:/Users/kiritani/desktop/datadescription.mat');
    data
    save('C:/Users/kiritani/desktop/alldata.mat', 'data', 'data_description', '-v7.3');
    T.Mouse_Name = categorical(T.Mouse_Name);
    mice = unique(T.Mouse_Name);
    for m = mice'
        mouse = T(T.Mouse_Name == m, :);
        data = table2struct(mouse, 'ToScalar', true);
        mkdir(['C:/Users/kiritani/Desktop/mice/', char(m)])
        %save(['C:/Users/kiritani/Desktop/mice/', char(m), '/', char(m)], 'data', 'data_description');
    end
end